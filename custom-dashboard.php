<?php
/**
 * Plugin Name: Bona Dashboard 
 * Description: Custom Dashboard
 * Version: 3.0.1
 * Author: Bonaecom Team
 */

add_action("admin_menu", "bona_general_add_options_submenu");
function bona_general_add_options_submenu() {
  add_submenu_page(
        'options-general.php',
        'Prefix Order',
        'Prefix Order',
        'administrator',
        'prefix-order',
        'bona_general_add_options_submenu_callback' );
}
function bona_general_add_options_submenu_callback(){
	require __DIR__ .'/templates/general_setting.php';	
}


if(! function_exists( 'bona_general_settings_register_fields' ) ){
	// add_filter('admin_init', 'bona_general_settings_register_fields');
	function bona_general_settings_register_fields()
	{
		register_setting('general', 'sequential_order_numbers', 'esc_attr');
		add_settings_field('sequential_order_numbers', '<label for="sequential_order_numbers">'.__('Sequential Order Numbers' , 'sequential_order_numbers' ).'</label>' , 'bona_general_settings_order_number_html', 'general');
		register_setting('general', '_prefix_order', 'esc_attr');
		add_settings_field('_prefix_order', '<label for="prefix_order">'.__('Prefix Order' , 'prefix_order' ).'</label>' , 'bona_general_settings_prefix_order_html', 'general');
		
		register_setting('general', 'reset_order_numbers', 'esc_attr');
		add_settings_field('reset_order_numbers', '<label for="reset_order_numbers">'.__('Reset Order Numbers Every Month' , 'reset_order_numbers' ).'</label>' , 'bona_general_settings_reset_order_number_html', 'general');
	}
	function bona_general_settings_reset_order_number_html()
	{
		$value = get_option( 'reset_order_numbers');
		if (FALSE === $value){
			$check = 'checked';
		}
		else{
		
			$check = ($value == 'on' ? 'checked' : '');
		}
			
		
		echo '<input type="checkbox" id="reset_order_numbers" name="reset_order_numbers" ' . $check . ' />';
		
	}
	function bona_general_settings_order_number_html()
	{
		$value = get_option( 'sequential_order_numbers', '' );
		echo '<input type="text" id="sequential_order_numbers" name="sequential_order_numbers" value="' . $value . '" class="regular-text ltr" readonly />';
		echo '<input type="checkbox" id="sequential_order_numbers_change" name="sequential_order_numbers_change" class="regular-text ltr" style="margin-left:10px" /> <label>Update Sequential Order Numbers</label>';
		
	}
	function bona_general_settings_prefix_order_html()
	{
		$value = get_option( '_prefix_order', '' );
		echo '<input type="text" id="prefix_order" name="_prefix_order" value="' . $value . '" class="regular-text ltr" />';
		
	}
}
if(! function_exists( 'ecommerce_update_order_number' ) ){
	//add_action( 'woocommerce_checkout_update_order_meta', 'ecommerce_update_order_number' , 10, 1);
	function ecommerce_update_order_number ( $order_id ) {
		$order_number = get_option( 'sequential_order_numbers' );
		if (FALSE === $order_number) add_option('sequential_order_numbers','1');
		
		add_post_meta( $order_id, '_order_number',  $order_number);
		update_option('sequential_order_numbers',(int)$order_number+1);
	}
}
if(! function_exists( 'update_woocommerce_order_number' ) ){
	add_filter( 'woocommerce_order_number', 'update_woocommerce_order_number' );
	
	function update_woocommerce_order_number( $order_id ) {
		if ( ! $order_id ){
			$order_number = get_option( 'sequential_order_numbers' );
			$prefix_order = get_option( '_prefix_order' );
			$month_order = get_option( '_month_order' );
			$reset_order_numbers = get_option( 'reset_order_numbers' );
			if (FALSE === $order_number) {
				add_option('sequential_order_numbers','1');
				$order_number = 1;
				
			}
			if (FALSE === $prefix_order) {
				add_option('_prefix_order','BD');
				$prefix_order = 'BD';
				
			}
			if (FALSE === $month_order) {
				add_option('_month_order',date('m'));
				$month_order = date('m');
				
			}
			if(FALSE === $reset_order_numbers || $reset_order_numbers == 'on'){
				if($month_order !== date('m')){
					$order_number = 1;
				}
			}
			if($month_order !== date('m')){
				update_option('_month_order',date('m'));
				
			}
			return $prefix_order.'/'.date('Y').'/'.date('m').'/'.sprintf("%04d", $order_number);
		}
		
		$new_order_id = get_post_meta($order_id,'_order_number',true);
		return ($new_order_id !== '' ? $new_order_id : $order_id);
	}
}
if(! function_exists( 'ecommerce_shop_order_search_order_number' ) ){
	function ecommerce_shop_order_search_order_number( $search_fields ) {
	
	  $search_fields[] = '_order_number';
	  
	
	  return $search_fields;
	}
	
	add_filter( 'woocommerce_shop_order_search_fields', 'ecommerce_shop_order_search_order_number' );
}
add_action('admin_enqueue_scripts', 'add_more_widgets_script'); //add plugin script;

function add_more_widgets_script(){

		 $my_current_screen = get_current_screen();
 
        // if ( isset( $my_current_screen->base ) && 'dashboard' === $my_current_screen->base ) {	
		
		wp_enqueue_style( 'jquery-ui-css', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', '', '1.12.1' );
			wp_enqueue_script('more-widgets', plugins_url( '/jquery.js' , __FILE__ ), array('jquery'), 1.0, true);
			wp_enqueue_script('custom-dashboard-js', plugins_url( '/custom.js' , __FILE__ ), array('jquery'), 1.0, true);
		
		// }
		
		wp_localize_script('more-widgets', 'more_widgets', array('ajaxurl' => admin_url('admin-ajax.php')));
	
}
add_action('wp_ajax_sale_analysis', 'get_sale_analysis'); //fire get_more_posts on AJAX call for logged-in users;
add_action('wp_ajax_nopriv_sale_analysis', 'get_sale_analysis'); //fire get_more_posts on AJAX call for all other users;

function get_sale_analysis(){	

	$period = ((isset($_GET['period']) and $_GET['period'] !== '') ? $_GET['period'] : 'year');
	if($period == 'year'){
        $date= ((isset($_GET['year_select']) and $_GET['year_select'] !== '') ? $_GET['year_select'] : date('Y'));
    }
    elseif($period == 'month'){
        $month = ((isset($_GET['month_select']) and $_GET['month_select'] !== '') ? $_GET['month_select'] : date('Y-m'));
        $date= $month. '-01';
    }
    else{
        $date = ((isset($_GET['day_select']) and $_GET['day_select'] !== '') ? $_GET['day_select']  : date('d-m-Y',strtotime(time())));
        $array_date=explode('-',$date);
        if(is_array($array_date) and count($array_date) > 0){
    		if(count($array_date) == 1) {
    			$day = '01';
    			$month = '01';
    			$year = $array_date[0];
    		}
    		if(count($array_date) == 2) {
    			$day = '01';
    			$month = $array_date[1];
    			$year = $array_date[0];
    		}
    		if(count($array_date) == 3) {
    			$day = $array_date[0];
    			$month = $array_date[1];
    			$year = $array_date[2];
    		}
    		$date= $year.'-'.$month.'-'.$day;
    	}
    }
	global $wpdb;
	if($period == 'year'){
		$result = $wpdb->get_results( "SELECT ID, DATE_FORMAT(`post_date`,'%Y-%m-%d') as p_date FROM $wpdb->posts 
				WHERE post_type = 'shop_order'				
				AND post_status not in ('auto-draft','trash','wc-failed','wc-refunded','wc-cancelled')
				AND year(post_date) = ".$date."
				order by p_date
			");
            
	}
	if($period == 'month'){
		
		$result = $wpdb->get_results( "SELECT ID, DATE_FORMAT(`post_date`,'%Y-%m-%d') as p_date FROM $wpdb->posts 
				WHERE post_type = 'shop_order'				
				AND month(post_date) = month('".$date."') and year(post_date) = year('".$date."')
				AND post_status not in ('auto-draft','trash','wc-failed','wc-refunded','wc-cancelled')
				order by p_date
			");
			
	}
	if($period == 'week'){
		$result = $wpdb->get_results( "SELECT ID, DATE_FORMAT(`post_date`,'%Y-%m-%d') as p_date FROM $wpdb->posts 
				WHERE post_type = 'shop_order'				
				AND week(post_date) = week('".$date."') and month(post_date) = month('".$date."') and year(post_date) = year('".$date."')
				AND post_status not in ('auto-draft','trash','wc-failed','wc-refunded','wc-cancelled')
				order by p_date
			");
            
	}
	
	$data = array();
	
  	if(count($result) > 0){
		$date_array= [];
		$total_array= [];
		
		foreach($result as $key => $value){
			
			$order = wc_get_order( $value->ID);
	  		
			// Now you have access to (see above)...
			  
			if ( $order ) {
				if(in_array($value->p_date,$date_array)){
					$i=0;
					foreach($date_array as $d){
						if($d==$value->p_date){
							$total_array[$i] += $order->get_total();	
						}
						$i+=1;
					}
				}
				else{
					array_push($date_array,$value->p_date);
					array_push($total_array,$order->get_total());
				}
			   /*$data[]=array(
					'date' => date('Y-m-d',strtotime($value->p_date)),
					'total'=> $order->get_total()
				);*/
			   
			}
			
		}
        if(is_array($date_array) and count($date_array) > 0){
            $j=0;
            foreach ($date_array as $date){
                $data[]=array(
					'date' => date('Y-m-d',strtotime($date)),
					'total'=> $total_array[$j]
				);
              $j+=1;  
            }
        }
	
	}
	//https://dyclassroom.com/chartjs/chartjs-how-to-draw-line-graph-using-data-from-mysql-table-and-php
	//var_dump($data);
	echo ( json_encode($data));
	exit();
}
add_action('wp_dashboard_setup', 'anna_custom_dashboard_widgets');
  
function anna_custom_dashboard_widgets() {
	global $wp_meta_boxes;
 	//wp_add_dashboard_widget('export_widget', 'Export', 'anna_export');
	wp_add_dashboard_widget('custom_help_widget', 'Summary Report', 'anna_custom_dashboard_help');
	wp_add_dashboard_widget('latest_customer_order_widget', 'New Customer / Order', 'anna_new_customer_order');
	wp_add_dashboard_widget('sale_analysis_widget', 'Sales Analysis', 'anna_custom_sale_analysis');
	
}
function user_count() { 
	$usercount = count_users();
	$result = $usercount['total_users']; 
	return $result; 
}
function get_total_orders(){
	global $wpdb;

	$post_status = implode("','", array('wc-processing', 'wc-completed') );
	
	$result = $wpdb->get_var( "SELECT count(*) FROM $wpdb->posts 
				WHERE post_type = 'shop_order'
				AND post_status not in ('auto-draft','trash','wc-failed','wc-refunded','wc-cancelled')
				AND year(post_date) = year(now())
			");
	
	return $result;
}

//woocommerce get total sale count of a product after a specific data
function get_total_sold_by()
{
    global $wpdb;

	$post_status = implode("','", array('wc-processing', 'wc-completed') );
	
	$result = $wpdb->get_results( "SELECT * FROM $wpdb->posts 
				WHERE post_type = 'shop_order'
				AND post_status not in ('auto-draft','trash','wc-failed','wc-refunded','wc-cancelled')	
				AND year(post_date) = year(now())			
			");
	$total = 0;
	//var_dump($result);
	//return ;
  	if(count($result) > 0){
		foreach($result as $key => $value){
			$order = wc_get_order( $value->ID);
	  
			// Now you have access to (see above)...
			  
			if ( $order ) {
			   $total+=$order->get_total();
			   
			}	
		}
	}
	
    return number_format($total,2);
}
function anna_export() {
	echo '<p><input type="button" class="export" value="Export to Excel"></p>';
}
function anna_custom_dashboard_help() {
	$count_user = get_option('count_users',false);
	$online_users = 0;
	if($count_user !== false){
		$count_user = unserialize($count_user);
		if($count_user !== false and is_array($count_user) and count($count_user) > 0){
			//var_dump($count_user);
			foreach($count_user as $key => $value){
				if($value['time'] >= time()-60*60*24 and $value['user_id'] <> 0){
					$online_users +=1;
				}
				else{
					unset(	$count_user[$key]);
				}
			}
			update_option('count_users',serialize($count_user));
		}
	}
    if ( !function_exists( 'count_users' ) ) { 
        require_once ABSPATH . WPINC . '/user.php'; 
    } 
      
    // 'time' or 'memory' 
    $strategy = 'time'; 
      
    // NOTICE! Understand what this does before running. 
    $online_users = count_users($strategy);
    //var_dump($online_users);
	//var_dump(gearside_online_users(''));
echo '
<style>
.summarize div{
		width: 20%;
		background: #eee;
		/*margin-right: 10px;*/
		margin: 0 2.2%;
		display: inline-block;
		
		border: 1px solid #ccc;
		border-radius: 5px;
   }
   
   .summarize table{
		width: 100%;  
		padding: 10px; 
   }
   .summarize table td{
		width: 50%; 
		font-size: 30px; 
		vertical-align: top; 
   }
   .summarize table td:last-child{
		text-align: right;   
   }
   .summarize .dashicons{
		font-size: 48px;   
   }
   .summarize p{
		padding-left: 10px;   
   }
   .summarize a{
		text-decoration: none;   
		color: #333!important;
   }
   .summarize a:hover{
	   text-decoration: underline;
   }
   .summarize h3{
		text-transform: uppercase;
		padding: 10px!important;
		background: #666;
		color: #fff!important;
		   
   }
@media screen and (max-width: 1024px){
	   .summarize div{
		width: 45%;
        margin-bottom: 15px;
	   }
   }
   @media screen and (max-width: 768px){
	   .summarize div{
		width: 95%;
        margin-bottom: 15px;
	   }
   }
</style>
<div class="summarize">

	
	<div class="order">
		
			
		
		<h3>Total Orders</h3>
		<table class="table">
			<tr>
				<td>
				<span class="dashicons dashicons-cart"></span>
				</td>
				<td>
				'. get_total_orders() .'
				</td>
				
			</tr>
		</table>
		<p><a href="'.home_url().'/wp-admin/edit.php?post_type=shop_order" target="_blank">View More...</a></p>
	</div>
	<div class="order">
		<h3>Total Sales</h3>
		<table class="table">
			<tr>
				<td>
				<span class="dashicons dashicons-money-alt"></span>
				</td>
				<td>
				'. get_total_sold_by() .'
				</td>
				
			</tr>
		</table>
		<p><a href="'.home_url().'/wp-admin/edit.php?post_type=shop_order" target="_blank">View More...</a></p>
	</div>
	<div class="customer">
		<h3>Total Customers</h3>
		<table class="table">
			<tr>
				<td>
				<span class="dashicons dashicons-buddicons-buddypress-logo"></span>
				</td>
				<td>
				'. user_count() .'
				</td>
				
			</tr>
		</table>
		<p><a href="'.home_url().'/wp-admin/users.php" target="_blank">View More...</a></p>
	</div>
	<div class="customer">
		<h3>Total Customers Online</h3>
		<table class="table">
			<tr>
				<td>
				<span class="dashicons dashicons-buddicons-buddypress-logo"></span>
				</td>
				<td>
				'. gearside_online_users('count') .
				//$online_users['avail_roles']['customer'].'
				'</td>
				
			</tr>
		</table>
		<p><a href="'.home_url().'/wp-admin/users.php" target="_blank">View More...</a></p>
	</div>
	</div>
	<div style="display:none">';
	global $wpdb;
	$result = $wpdb->get_results( "SELECT ID, DATE_FORMAT(`post_date`,'%Y-%m-%d') as p_date FROM $wpdb->posts 
				WHERE post_type = 'shop_order'				
				AND post_status not in ('auto-draft','trash','wc-failed','wc-refunded','wc-cancelled')
				AND year(post_date) = 2021
				order by p_date
			");
			$data=array();
  	if(count($result) > 0){
		foreach($result as $key => $value){
			$order = wc_get_order( $value->ID);
	  		echo $value->ID;
			// Now you have access to (see above)...
			  //echo date('Y-m-d',strtotime($value->p_date)).'<br />';
			if ( $order ) {
			   $data[]=array(
					'date' => date('Y-m-d',strtotime($value->p_date)),
					'total'=> $order->get_total()
				);
			   
			}
			
		}
		var_dump($data);
	}
	echo '</div>';
}
//Update user online status
add_action('init', 'gearside_users_status_init');
add_action('admin_init', 'gearside_users_status_init');
function gearside_users_status_init(){
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	$user = wp_get_current_user(); //Get the current user's data

	//Update the user if they are not on the list, or if they have not been online in the last 900 seconds (15 minutes)
	if ( !isset($logged_in_users[$user->ID]['last']) || $logged_in_users[$user->ID]['last'] <= time()-900 ){
		$logged_in_users[$user->ID] = array(
			'id' => $user->ID,
			'username' => $user->user_login,
			'last' => time(),
		);
		set_transient('users_status', $logged_in_users, 900); //Set this transient to expire 15 minutes after it is created.
	}
}

//Check if a user has been online in the last 15 minutes
function gearside_is_user_online($id){	
	$logged_in_users = get_transient('users_status'); //Get the active users from the transient.
	
	return isset($logged_in_users[$id]['last']) && $logged_in_users[$id]['last'] > time()-900; //Return boolean if the user has been online in the last 900 seconds (15 minutes).
}
function gearside_online_users($return){
	$logged_in_users = get_transient('users_status');
	
	//If no users are online
	if ( empty($logged_in_users) ){
		return ( $return == 'count' )? 0 : false; //If requesting a count return 0, if requesting user data return false.
	}
	
	$user_online_count = 0;
	$online_users = array();
	foreach ( $logged_in_users as $user ){
		if ( !empty($user['username']) && isset($user['last']) && $user['last'] > time()-900 ){ //If the user has been online in the last 900 seconds, add them to the array and increase the online count.
			$online_users[] = $user;
			$user_online_count++;
		}
	}

	return ( $return == 'count' )? $user_online_count : $online_users; //Return either an integer count, or an array of all online user data.
}
function anna_custom_sale_analysis(){
    $start = date('Y') - 10;
	$end = date('Y');
	$select_year ='';
	while ($start <= $end) {
      $select_year.= "<option>{$end}</option>";
      $end--;
  }
	echo '<div class="chart-container">
	
	<select class="option-type">
		<option value="">Choose Period</option>
		<option value="year">Year</option>
		<option value="month">Month</option>
		<option value="week">Week</option>
	</select>
	<input type="text" id="datepicker" class="select-date" value="" placeholder="Select a date"/> 
	<input type="month" class="select-month" value="" /> 
	<select class="select-year">
	'.$select_year.'
	</select> 
	<button type="button" class="button apply">Apply</button>
      <canvas id="myreport"></canvas>
    </div>';
	
	//var_dump( is_user_online(0).'testing');
}
function get_recently_registered_users() { 
 
global $wpdb;
 
$recentusers = '<ul class="recently-user">';
 
$usernames = $wpdb->get_results("SELECT ID, display_name, user_url, user_email, user_registered FROM $wpdb->users ORDER BY ID DESC LIMIT 1");
 
foreach ($usernames as $username) {
 

 $recentusers .= '<li><a target="_blank" href="'.home_url().'/wp-admin/user-edit.php?user_id='.$username->ID.'">'.$username->display_name."</a><br> on ". date('l, d F Y',strtotime($username->user_registered))."</li>";
//$recentusers .= '<li>' .get_avatar($username->user_email, 45).' <a target="_blank" href="'.home_url().'/wp-admin/user-edit.php?user_id='.$username->ID.'">'.$username->display_name."</a><br> on ". date('l, d F Y',strtotime($username->user_registered))."</li>";
 
}
$recentusers .= '</ul>';
 
return $recentusers;  
}

function get_recently_order_purchase() { 
 
global $wpdb;
 
$recentorders = '<ul class="recently-orders">';
 
$orders = $wpdb->get_results("SELECT ID, post_title, post_status, guid, post_date FROM $wpdb->posts where post_type='shop_order' and post_status not in ('auto-draft','wc-refunded','wc-failed','trash','wc-cancelled') ORDER BY ID DESC LIMIT 1");
 
foreach ($orders as $order) {
 
$detail = wc_get_order( $order->ID);
 
$recentorders .= '<li><a target="_blank" href="'.home_url().'/wp-admin/post.php?post='.$order->ID.'&action=edit">'.$order->post_title."</a><br>Amount: $". $detail->get_total()."</li>";
 
}
$recentorders .= '</ul>';
 
return $recentorders;  
}
function anna_new_customer_order(){
	echo '<table class="table" width="100%">
	<tr>
	<th style="text-align:left;">Customer</th>
	<th style="text-align:left;">Order</th>
	</tr>
		<tr>
			<td>
			'.get_recently_registered_users().'
			</td>
			<td>
			'.get_recently_order_purchase().'
			</td>
		</tr>
	</table>';
	//var_dump( is_user_online(0).'testing');
}

add_action('wp_login', 'update_user_loggedin');
function update_user_loggedin() {
	$user = wp_get_current_user();
    //Get current user
    $id = $user->ID;
	$log = array();
    if (FALSE === get_option('count_users') && FALSE === update_option('count_users',FALSE)) {
		$log[]=array(
			'user_id' => $id,
			'time' => time()
		);
		add_option('count_users',serialize($log));
	}
	else{
		if(get_option('count_users') !== FALSE){
			$log = (unserialize(get_option('count_users')) !== false ? unserialize(get_option('count_users')) : array());
			$log[]=array(
				'user_id' => $id,
				'time' => time()
			);
			//$count = get_option('count_users');
			update_option('count_users',serialize($log));
				
		}
	}	

}
function update_user_loggedout() {
	$user = wp_get_current_user();
    //Get current user
    $id = $user->ID;
	//echo $id;
	$log = array();
    if (FALSE === get_option('count_users') && FALSE === update_option('count_users',FALSE)) {
		
	}
	else{
		if(get_option('count_users') !== FALSE){
			$log = unserialize( get_option('count_users'));
			if($log !== false and is_array($log) and count($log) > 0){
				foreach($log as $key => $value){
					if($value['user_id'] == $id){
						unset($log[$key]);	
						//var_dump($log);
					}
				}
			}
			else{
				$log = array();	
			}
			update_option('count_users',serialize($log));
				
		}
	}	

}
add_action('wp_logout', 'update_user_loggedout');
/**
 * Save post metadata when a post is saved.
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function bona_save_order_meta( $post_id, $post, $update ) {
	/*
     * In production code, $slug should be set only once in the plugin,
     * preferably as a class property, rather than in each function that needs it.
     */
    $post_type = get_post_type($post_id);
	$post_status = get_post_status($post_id);
	if($post_type == 'shop_order'){
		if(get_post_meta($post_id, '_order_number', true)){
		}
		else{
			if($post_status !== 'auto-draft'){
				$order_number = get_option( 'sequential_order_numbers' );
				$prefix_order = get_option( '_prefix_order' );
				$month_order = get_option( '_month_order' );
				$reset_order_numbers = get_option( 'reset_order_numbers' );
				if (FALSE === $order_number) {
					add_option('sequential_order_numbers','1');
					$order_number = 1;
					
				}
				if (FALSE === $prefix_order) {
					add_option('_prefix_order','BD');
					$prefix_order = 'BD';
					
				}
				if (FALSE === $month_order) {
					add_option('_month_order',date('m'));
					$month_order = date('m');
					
					
				}
				if(FALSE === $reset_order_numbers || $reset_order_numbers == 'on'){
					if($month_order !== date('m')){
						$order_number = 1;
						
					}
				}
				if($month_order !== date('m')){
					update_option('_month_order',date('m'));
					
				}
				
				add_post_meta( $post_id, '_order_number',  $prefix_order.'/'.date('Y').'/'.date('m').'/'.sprintf("%04d", $order_number));
				update_option('sequential_order_numbers',(int)$order_number+1);
			}
		}
		
				
	}
}
add_action( 'save_post', 'bona_save_order_meta', 10, 3 );