<?php
$sequential_order_numbers = get_option( 'sequential_order_numbers', '' );
$prefix_order = get_option( '_prefix_order', '' );
$reset_order_numbers = get_option( 'reset_order_numbers');
if ($reset_order_numbers === FALSE){
    $reset_check = 'checked';
}else{
    $reset_check = ($reset_order_numbers == 'on' ? 'checked' : '');
}

if(isset($_POST['save'])){
    if($_POST['sequential_order_numbers_change'] == 'on'){
        if(get_option('sequential_order_numbers', '') == ''){
            add_option(	'sequential_order_numbers', $_POST['sequential_order_numbers']);
        }else{
            update_option(	'sequential_order_numbers', $_POST['sequential_order_numbers']);
        }
    }
    if(get_option('_prefix_order', '') == ''){
        add_option(	'_prefix_order', $_POST['prefix_order']);
    }else{
        update_option(	'_prefix_order', $_POST['prefix_order']);
    }
    if(get_option('reset_order_numbers') === false){
        add_option(	'reset_order_numbers', $_POST['reset_order_numbers']);
    }else{
        update_option(	'reset_order_numbers', $_POST['reset_order_numbers']);
    }
    ?><script>window.location.reload();</script><?php
}
?>

<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

<h3>Prefix Order Setting</h3></br>
<form name="normal-court" method="post">
    <table class="table">
        <tr>
            <td><strong>Sequential Order Numbers</strong></td>
            <td>
                <input style="width:200px" type="text" id="sequential_order_numbers" name="sequential_order_numbers" value="<?php echo $sequential_order_numbers ?>" class="regular-text ltr" readonly />
                <input type="checkbox" id="sequential_order_numbers_change" name="sequential_order_numbers_change" class="regular-text ltr" style="margin-left:10px" /> <label for="sequential_order_numbers_change"><small>Update Sequential Order Numbers</small></label>
            </td>
        </tr>
        <tr>
            <td><strong>Prefix Order</strong></td>
            <td>
                <input type="text" id="prefix_order" name="prefix_order" value="<?php echo $prefix_order ?>" class="regular-text ltr" />
            </td>
        </tr>
        <tr>
            <td><strong>Reset Order Numbers Every Month</strong></td>
            <td>
                <input type="checkbox" id="reset_order_numbers" name="reset_order_numbers" <?php echo $reset_check ?> />
            </td>
        </tr>
    </table>
    <br />
	<input type="submit" class="button" name="save" value="Save Changes" />
</form>