( function( $ ) {
	function change_period(){
		//$("#datepicker").datepicker("destroy")
		/*if(jQuery(".option-type option:selected").val() == 'year'){
				
			jQuery('#datepicker').datepicker(
				{ 
					changeMonth: false,
            		changeYear: true,
					dateFormat: "yy" 
				}
			);
		}
		else if(jQuery(".option-type option:selected").val() == 'month'){
				
			jQuery('#datepicker').datepicker(
				{ 
					changeMonth: true,
            		changeYear: true,
					dateFormat: "mm-yy" 
				}
			);
		}
		else{
			jQuery('#datepicker').datepicker(
				{ 
					dateFormat: "dd-mm-yy" 
				}
			);
		}*/
        if(jQuery(".option-type option:selected").val() == 'year'){
				
			jQuery('#datepicker').hide();
			jQuery('.select-month').hide();
			jQuery('.select-year').show();
		}
		else if(jQuery(".option-type option:selected").val() == 'month'){
				
			jQuery('#datepicker').hide();
			jQuery('.select-month').show();
			jQuery('.select-year').hide();
		}
		else{
			jQuery('#datepicker').show();
			jQuery('.select-month').hide();
			jQuery('.select-year').hide();
		}
	}
	$(document).ready(function($){
	   jQuery('#datepicker').datepicker(
			{ 
				dateFormat: "dd-mm-yy" 
			}
		);
		jQuery(".export").click(function(e){
			var part1 = $('.summarize').html();
			var part3 = '<h3>Woocommerce Status</h3>';//$('.chart-container').html();
			var part2 = $('.wc_status_list').html()
		  window.open('data:application/vnd.ms-excel,' + encodeURIComponent( part1+part3+part2));
    		e.preventDefault();
		});
		change_period();
		jQuery(".option-type").change(function(){
			change_period();
		});
		jQuery(".apply").click(function(){
			var day_select = $('.select-date').val();
			var month_select = $('.select-month').val();
			var year_select = $('.select-year option:selected').val();
			
			var period = $('.option-type').val();
		  	$.ajax({
			url: more_widgets.ajaxurl,
			type: "GET",
			data: {
				period: period,					
				day_select: day_select,
				month_select: month_select,
				year_select: year_select,
				action: 'sale_analysis',
				
			
			},
			dataType: "json",
			success: function(data){					
				console.log(data);

				  var date_buy = [];
				  var total = [];
				  //var result = JSON.parse(data);
				  /*if(Array.isArray(result)){
				  for (var i = 0; i < result.length; i++) {
					  date_buy.push(result[i].date);
					  total.push(result[i].total);
					}
				  }*/
				  for(var i in data) {
					//console.log(data[i]);
					date_buy.push(data[i].date);
					total.push(data[i].total);
					
				  }
					
				  var chartdata = {
					labels: date_buy,
					datasets: [
					  {
						label: "Amount",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(59, 89, 152, 0.75)",
						borderColor: "rgba(59, 89, 152, 1)",
						pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
						pointHoverBorderColor: "rgba(59, 89, 152, 1)",
						data: total
					  },
					  
					]
				  };
			
				  var ctx = $("#myreport");
			
				  var LineGraph = new Chart(ctx, {
					type: 'line',
					data: chartdata
				  });
			}
		});
		  
		  });
		  var currentDate = new Date()
			var day = currentDate.getDate()
			var month = currentDate.getMonth() + 1
			var year = currentDate.getFullYear()
			
		$.ajax({
			url: more_widgets.ajaxurl,
			type: "GET",
			data: {
				period: 'year',	
                				
				day_select: month+'-'+day+'-'+year,
				month_select: year+'-'+month,
				year_select: year,				
				
				action: 'sale_analysis',
				
			
			},
			dataType: "json",
			success: function(data){					
				//console.log(data);

				  var date_buy = [];
				  var total = [];
				  //var result = JSON.parse(data);
				  /*if(Array.isArray(result)){
				  for (var i = 0; i < result.length; i++) {
					  date_buy.push(result[i].date);
					  total.push(result[i].total);
					}
				  }*/
				  for(var i in data) {
					//console.log(data[i]);
					date_buy.push(data[i].date);
					total.push(data[i].total);
					
				  }
					
				  var chartdata = {
					labels: date_buy,
					datasets: [
					  {
						label: "Amount",
						fill: false,
						lineTension: 0.1,
						backgroundColor: "rgba(59, 89, 152, 0.75)",
						borderColor: "rgba(59, 89, 152, 1)",
						pointHoverBackgroundColor: "rgba(59, 89, 152, 1)",
						pointHoverBorderColor: "rgba(59, 89, 152, 1)",
						data: total
					  },
					  
					]
				  };
			
				  var ctx = $("#myreport");
			
				  var LineGraph = new Chart(ctx, {
					type: 'line',
					data: chartdata
				  });
			}
		});
	});

	$(document).ready(function($){
		$("#sequential_order_numbers_change").change(function() {
			if(this.checked) {
				$('#sequential_order_numbers').attr('readonly', false);
			}else{
				$('#sequential_order_numbers').attr('readonly', true);
			}
		});
	});

} )( jQuery );